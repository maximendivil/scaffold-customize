﻿using Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SolucionBaseEFCodeFirst.DAL
{
    public class SlnBaseEFCodeFirst : DbContext
    {
        public SlnBaseEFCodeFirst() : base("SlnBaseEFCodeFirstContext")
        {

        }

        public DbSet<Product> Products { get; set; }

        public DbSet<Category> Categories { get; set; }
    }
}